from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json

def get_photo(city, state):
    # Create a dictionary for the headers to use in the request
    headers = {'Authorization': PEXELS_API_KEY}
    # Create the URL for the request with the city and state
    response = requests.get(f"https://api.pexels.com/v1/search?query={city}+{state}", headers=headers)
    # Parse the JSON response
    picture = json.loads(response.content)
    # print("Response:")
    # print(picture)
    # Return a dictionary that contains a `picture_url` key and
    #   one of the URLs for one of the pictures in the response
    picture_info = {
        "picture_url": f'{picture["photos"][0]["url"]}',
    }
    return picture_info


def get_weather_data(city, state):

    headers1 = {
        "q": f"{city},{state},US",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY
    }
    response = requests.get(f"http://api.openweathermap.org/geo/1.0/direct",
                        params=headers1)
    geo = json.loads(response.content)

    try:
        latitude = geo[0]["lat"]
        longitude = geo[0]["lon"]
    except (KeyError, IndexError):
        return None

    headers2 = {
        "lat": latitude,
        "lon": longitude,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial"
    }
    response = requests.get(
        "https://api.openweathermap.org/data/2.5/weather",
        params=headers2
    )
    weather = json.loads(response.content)
    print(weather)
    try:
        return {
            "description": weather["weather"][0]["description"],
            "temp": weather["main"]["temp"],
        }
    except (KeyError, IndexError):
        return None
    return weather["weather"]
