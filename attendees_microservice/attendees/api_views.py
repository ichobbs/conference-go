from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Attendee, Badge, ConferenceVO, AccountVO
from django.views.decorators.http import require_http_methods
import json

"""
In attendees_microservice/attendees/api_views.py, we need to get rid
of all imports from the events module. Then, add ConferenceVO to the
list of things imported from the .models module.

Add this new encoder to the file:
"""


class ConferenceVODetailEncoder(ModelEncoder):
    model = ConferenceVO
    properties = ["name", "import_href"]


class BadgeEncoder(ModelEncoder):
    model = Badge
    properties = []


class AttendeeEncoder(ModelEncoder):
    model = Attendee
    properties = ["name"]


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
        "conference",
    ]
    encoders = {
        "conference": ConferenceVODetailEncoder(),
    }

    def get_extra_data(self, o):
        count = AccountVO.objects.filter(email=o.email).count()
        return {"has_account": count > 0}


@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_vo_id):
    """
    Lists the attendees names and the link to the attendee
    for the specified conference id.

    Returns a dictionary with a single key "attendees" which
    is a list of attendee names and URLS. Each entry in the list
    is a dictionary that contains the name of the attendee and
    the link to the attendee's information.
    """
    if request.method == "GET":
        # This filter is correct, with double _ and a single =
        attendee_list = Attendee.objects.filter(
            conference=conference_vo_id
        )

        return JsonResponse(
            {"attendees": attendee_list},
            encoder=AttendeeEncoder,
            safe=False,
        )

    else:
        content = json.loads(request.body)

        try:
            # THIS LINE IS ADDED
            conference_href = f"/api/conferences/{conference_vo_id}/"

            # THIS LINE CHANGES TO ConferenceVO and import_href
            conference = ConferenceVO.objects.get(import_href=conference_href)

            content["conference"] = conference

            ## THIS CHANGES TO ConferenceVO
        except ConferenceVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )

        attendee = Attendee.objects.create(**content)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )


def api_show_attendee(request, id):
    """
    Returns the details for the Attendee model specified
    by the id parameter.

    This should return a dictionary with email, name,
    company name, created, and conference properties for
    the specified Attendee instance.
    """
    attendee = Attendee.objects.get(id=id)
    return JsonResponse(
        attendee,
        encoder=AttendeeDetailEncoder,
        safe=False,
    )
